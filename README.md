# postfix-dovecot-mysql

Modifies the database layout from the Linode article «Email with Postfix, Dovecot, and MySQL» to flexible layout, with less maintenance.

Please follow the instructions at [Linode](https://www.linode.com/docs/email/postfix/email-with-postfix-dovecot-and-mysql) (alt. [raw text on github](https://raw.githubusercontent.com/linode/docs/master/docs/email/postfix/email-with-postfix-dovecot-and-mysql.md)), these files arent ment to replace anything directly in your setup - but more a guide.

Also have a read in the file `postfix.sql` and every other files before you're installing this, so you will get a grip on when you have to do something differently than explained in the tutorial.
